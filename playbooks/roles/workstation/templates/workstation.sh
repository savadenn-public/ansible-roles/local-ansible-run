#!/usr/bin/env bash

set -e
set -u

PROJECT_PATH={{ variable_projectPath }}
export PROJECT_PATH

cd "${PROJECT_PATH}"
git pull {{ variable_projectPath }}
docker compose run --rm ansible ansible-galaxy install -f --roles-path /app/.ansible/galaxy-roles -r requirements.yml
bash {{ variable_projectPath }}/requirements.sh
bash {{ variable_projectPath }}/workstation.sh