#!/usr/bin/env bash

arg=${@:-""}

set -e
set -u
set -o pipefail

NC='\033[0m'
ZEN_WIDTH=300
CONFIG_FILE="${PROJECT_PATH}/config"
. "${PROJECT_PATH}/.env"

function messageOK() {
    echo -e "\033[0;92m✓ $1${NC}"
}

function messageAction() {
    echo -e "\033[0;96m⚑ $1"
}

function messageEnd() {
    echo -e "${NC}"
}

function messageError() {
    echo -e "\033[0;91m✗ $1${NC}"
    zenity --error --text="$1" --width="${ZEN_WIDTH}"
}

function confirm() {
    set +e
    zenity --question --text="${1}" --width="${ZEN_WIDTH}"
    exit_code=$?
    set -e
    if [ "${exit_code}" != "0" ]; then
        messageError "Cancelled by user. Exiting"
        exit 1
    fi
}

#Load config
if [ -f "${CONFIG_FILE}" ]
then
  . "${CONFIG_FILE}"
else
  ROLES=''
fi


function optionActivated(){
  if [[ "" == "${ROLES}" || "${ROLES}" =~ (^|)"${1}"($|) ]]
  then
    echo TRUE "${1}"
  else
    echo FALSE "${1}"
  fi
}

# Dialog to set up configuration
ROLES=$(zenity --list --checklist \
  --title "Configurator" \
  --width="600" \
  --height="700" \
  --separator="|" \
  --column="Actif" --column="Role" --column="Definition" \
  $(optionActivated "workstation"         ) "Install this configurator as bin" \
  $(optionActivated "roles/networkManager") "Secure wifi configuration"  \
  $(optionActivated "roles/gitlfs"        ) "Git LFS plugin"  \
  $(optionActivated "roles/mkcert"        ) "Locally trusted root certificate for dev"  \
  $(optionActivated "roles/docker"        ) "Docker and local traefik reverse proxy"  \
  $(optionActivated "roles/minikube"      ) "Kubernetes  local for dev"  \
  $(optionActivated "roles/vscode"        ) "Microsoft VSCode IDE"  \
  $(optionActivated "roles/google-chrome" ) "Google chrome browser"  \
  $(optionActivated "language-tool"       ) "Local server for language-tool extension"  \
  $(optionActivated "roles/auto-afk"      ) "Go to AFK mumble channel  screen lock"  \
  $(optionActivated "roles/autoWifi"      ) "Swap between wifi and LAN i if cable is plugged"  \
  $(optionActivated "roles/element"       ) "Element (Matrix client)"  \
  $(optionActivated "roles/echo-cancel"   ) "Puleaudio echo canceling"  \
  $(optionActivated "roles/mumble"        ) "VOIP low latency client"  \
  $(optionActivated "roles/sublimeText"   ) "Install SublimeText editor"  \
  $(optionActivated "roles/terminal"      ) "Pimp your terminator"  \
  $(optionActivated "roles/yubilock"      ) "Lock your screen by unplugging your Yubikey"  \
)
echo "ROLES='${ROLES}'" > "${CONFIG_FILE}"

# Reset known_host to prevent docker-compose creating a directory
KNOWN_HOST_FILE=${PROJECT_PATH}/known_hosts_workstation
messageAction "Checking ${KNOWN_HOST_FILE}"
if [ -d "${KNOWN_HOST_FILE}" ]; then
  messageError "${KNOWN_HOST_FILE} should be a file, not a directory. We will remove it"
  messageAction "  [sudo required] Remove file"
  sudo rm -rf "${KNOWN_HOST_FILE}"
fi

true > "${KNOWN_HOST_FILE}"
messageOK "  OK"

## Start container to ensure network existence
messageAction "Docker networking"

DOCKER_HOST_IP=$(ip -4 addr show docker0 | grep -Po 'inet \K[\d.]+')
messageOK "  Docker host IP is ${DOCKER_HOST_IP}"

docker compose run --rm ansible sh -c 'echo OK' 1>/dev/null
subnet=$(docker network inspect "${COMPOSE_PROJECT_NAME}"_default -f '{{(index .IPAM.Config 0).Subnet}}')
messageOK "  Subnet IP range is ${subnet}"

network_id=$(docker network inspect -f {{.Id}} ${COMPOSE_PROJECT_NAME}_default)
messageOK "  Docker network id is ${network_id}"

network_id="br-${network_id:0:12}"
messageOK "  Bridge name is ${network_id}"

## Setting up firewall rules
messageAction "[sudo required] Setting up firewall rules"
sudo ufw allow in on "${network_id}" from "${subnet}"
messageOK "  ufw rule added"

## Start up sshd server
messageAction "[sudo required] Enables current user to access sshd as root"
sudo mkdir -p /root/.ssh
AUTH=/root/.ssh/authorized_keys
if sudo test -f "${AUTH}"; then
    messageError "There is a file ${AUTH}.

No root access should exist. This is a security concern. Verify the source of file in case of security breach.

  sudo cat /root/.ssh/authorized_keys

Then Delete it

  sudo rm -f /root/.ssh/authorized_keys"

    exit 1
fi

function authorizeSelf() {
  cat ~/.ssh/id_*.pub | sudo tee -a /root/.ssh/authorized_keys
  messageOK "/root/.ssh/authorized_keys created"
}

function exists() { [[ -f "$1" ]] ;}

if exists ~/.ssh/id_*.pub
then
  authorizeSelf
else
  confirm "No SSH keys detected, we are going to create one (ed25519). \n\nDo you agree (you may need to enter information in the console)?"
  ssh-keygen -t ed25519
  authorizeSelf
fi

messageAction "[sudo required] Starting sshd server"
sudo systemctl start sshd
messageOK "Server started"

messageAction "Get key of docker host"
ssh-keyscan -H "${DOCKER_HOST_IP}" >"${KNOWN_HOST_FILE}" 2>/dev/null
messageOK "${KNOWN_HOST_FILE} written"

## Run Ansible playbook
set +e
docker compose pull
docker compose build
docker compose run --rm --user "$(id -u):$(id -g)" \
    ansible ansible-playbook "${arg[@]}" \
    --extra-vars "variable_host=${DOCKER_HOST_IP}" \
    --extra-vars "variable_user=$(id -un)" \
    --extra-vars "variable_group=$(id -gn)" \
    --extra-vars "variable_projectPath=${PROJECT_PATH}" \
    --extra-vars "variable_loadedRoles='${ROLES}'" \
    playbooks/workstation.yml
exit_code=$?
set -e

messageAction "[sudo required] Stopping sshd server"
sudo systemctl stop sshd
messageOK "Server stopped"

messageAction "[sudo required] Delete root authorized keys"
sudo rm -f /root/.ssh/authorized_keys
messageOK "/root/.ssh/authorized_keys deleted"

## Setting up firewall rules
messageAction "[sudo required] Setting up firewall rules"
sudo ufw delete allow in on "${network_id}" from "${subnet}"
messageOK "  ufw rule added"

exit ${exit_code}
